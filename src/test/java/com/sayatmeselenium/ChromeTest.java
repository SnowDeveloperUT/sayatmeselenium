package com.sayatmeselenium;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Tests using Chrome Browser.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChromeTest {
    private static WebDriver driver;
    private TestCore testCore = new TestCore();

    // Opens the web browser before running the tests
    @BeforeClass
    public static void openBrowser() throws Exception {
        String driverName = "webdriver.chrome.driver";
        String driverPath = "C:/Users/Drivers/chromedriver.exe";

        System.out.println("\n\nUsing BrowserType: " + BrowserType.Chrome.name());

        System.setProperty(driverName, driverPath);
        driver = new ChromeDriver();
    }

    // Closes the browser once all the tests are executed
    @AfterClass
    public static void closeBrowser() {
        System.out.print("\nBrowser closed");
        driver.quit();
    }

    // Test case when user gives incorrect login credentials
    @Test(timeout = TestCore.testTimeout)
    public void testA_UserLoginOnFailure() throws InterruptedException {
        testCore.userLoginFail(driver, TestCore.websiteUrl);
    }

    // Test case when user logs in with correct login credentials and gives feedback to another user
    @Test(timeout = TestCore.testTimeout)
    public void testB_UserLoginOnSuccess() throws InterruptedException {
        testCore.userLoginOnSuccess(
                driver, TestCore.websiteUrl, TestCore.username, TestCore.password, TestCore.feedbackReceivingUser
        );
    }

    // Give feedback to another user without being logged in
    @Test(timeout = TestCore.testTimeout)
    public void testC_UserFeedbackOnWithoutLoggedIn() throws InterruptedException {
        testCore.userFeedbackOnWithoutLoggedIn(driver, TestCore.websiteUrl, TestCore.feedbackReceivingUser);
    }
}

package com.sayatmeselenium;

// Testing to be done in four different browsers
public enum BrowserType {
    Chrome,
    Firefox,
    Edge,
    IE
}

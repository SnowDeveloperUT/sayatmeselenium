package com.sayatmeselenium;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Test methods for SayAtMe web application
 */
class TestCore {

    private static final int threadSleepTime = 5000;
    static final int testTimeout = 2 * 60 * 1000;
    static final String websiteUrl = "https://kask6iktundubkorras.sayat.me";
    static final String username = "Hazel";
    static final String password = "hazel123";
    static final String feedbackReceivingUser = "harry";
    private static final String feedback = "Anything you would like :)";

    // When user gives incorrect login credentials
    void userLoginFail(WebDriver driver, String websiteUrl) throws InterruptedException {
        // -- Log in with a user with incorrect credentials--
        System.out.println("\n\n-- When user is logged in with incorrect login credentials --");
        driver.get(websiteUrl + "/login");

        // Wait for browser to load completely
        Thread.sleep(threadSleepTime);
        driver.findElement(By.id("fburl_d")).sendKeys("Hael");
        driver.findElement(By.id("login_passwd_d")).sendKeys("hazel123");

        By xpath = By.xpath("//form[contains(@class, 'front-login-form') and contains(@class, 'login')]" +
                "//button[contains(text(),'Log In')]");
        WebElement loginElement = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(xpath));

        loginElement.click();

        // Wait for browser to load completely
        Thread.sleep(threadSleepTime);

        // User is not allowed to log in and gets an alert of incorrect username/password
        Assert.assertTrue("User provided incorrect login credentials", driver.getPageSource().
                contains("Wrong feedback URL or password"));
        System.out.println("User cannot log in with incorrect login credentials");
    }

    // When user logs in with correct login credentials and gives feedback to another user
    void userLoginOnSuccess(
            WebDriver driver, String websiteUrl, String username, String password, String feedbackReceivingUser
    ) throws InterruptedException {
        // -- Log in with a user with correct credentials--
        driver.get(websiteUrl + "/login");
        driver.findElement(By.id("fburl_d")).sendKeys(username);
        driver.findElement(By.id("login_passwd_d")).sendKeys(password);

        By xpath = By.xpath("//form[contains(@class, 'front-login-form') and " +
                "contains(@class, 'login')]//button[contains(text(),'Log In')]");
        WebElement loginElement = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(xpath));

        loginElement.click();

        // Wait until browser loads completely
        Thread.sleep(threadSleepTime);

        // If there is correct user credentials then user logs in successfully
        System.out.println("\n\n-- When user is logged in with correct login credentials --");
        System.out.println("Logged in with a user successfully");
        Assert.assertTrue("User logged in successfully?", driver.getCurrentUrl().endsWith("read"));

        // -- Give feedback to another user when logged in --
        driver.findElement(By.name("q")).sendKeys(feedbackReceivingUser);//'q' is the 'name' element for the search box
        Thread.sleep(threadSleepTime);
        driver.findElement(By.name("q")).sendKeys(Keys.RETURN);

        // Checks for Feedback receiver's username
        if (driver.getPageSource().contains("Not found.")) {
            // In case the Feedback receiver's username is not found
            Assert.fail("\nTest failed: The user you wish to give feedback is not found! Please provide an " +
                    "existing user.");
        } else {
            Thread.sleep(threadSleepTime);
            // In case the Feedback receiver's username is found then we click on the username to give a feedback
            driver.findElement(By.linkText(feedbackReceivingUser)).click();
            Thread.sleep(threadSleepTime);

            // Give feedback to another user
            giveFeedback(driver, feedback);
            Thread.sleep(threadSleepTime);

            // Check if email confirmation pop up appears then we activate the saved screen
            if (!(driver.findElement(By.id("email-modal")).getSize().equals(0))) {
                driver.findElement(By.id("email-modal")).sendKeys(Keys.ESCAPE);
            }

            // -- Validate that the feedback was saved --
            Thread.sleep(threadSleepTime);
            Assert.assertTrue("User gave feedback successfully?", driver.getCurrentUrl().endsWith
                    ("#saved"));
            driver.get(websiteUrl + "/logout"); // Logout the user once the feedback is given
            System.out.println("Validation(Logged in) - Successful. Feedback to another user was given and saved " +
                    "successfully");
        }
    }

    // When user gives feedback without logging in to SayAtMe
    void userFeedbackOnWithoutLoggedIn(WebDriver driver, String websiteUrl, String feedbackReceiverUsername)
            throws InterruptedException {
        // Loads the url of Feedback receiver's username to give feedback
        driver.get(websiteUrl + "/" + feedbackReceiverUsername);
        Thread.sleep(threadSleepTime);

        // Checks if another Feedback receiver's username exists
        if (driver.getPageSource().contains("The selected page was not found.")) {
            // If Feedback receiver's username is not existing in SayAt.Me database
            System.out.println("\n\n-- When user is not logged in --");
            Assert.fail("\nTest failed: The user you wish to give feedback is not found! Please provide an existing " +
                    "user.");
        } else {
            // If Feedback receiver's username is found
            System.out.println("\n\n-- When user is not logged in --");

            // Give feedback to another user
            giveFeedback(driver, feedback);
            Thread.sleep(threadSleepTime);

            // -- Validate that the feedback was saved --
            Assert.assertTrue("User gave feedback successfully?", driver.getCurrentUrl().endsWith("#saved"));
            System.out.println("Validation(Not logged in) - Successful. Feedback to another user was given and saved " +
                    "successfully");
        }
    }

    // Give feedback to another user
    private void giveFeedback(WebDriver driver, String feedback) {
        driver.findElement(By.id("give-feedback-textarea")).sendKeys(feedback);
        driver.findElement(By.id("give-feedback-button")).sendKeys(Keys.RETURN);
        System.out.println("Feedback to another user given successfully");
    }
}

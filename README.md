# README #

Selenium test on a social network and anonymous feedback platform : http://kask6iktundubkorras.sayat.me/

### Selenium WebDriver project in Java using Maven ###


### Test Suite that would do the following tests  ###

* Log in with a user
	* Login with a user with incorrect login credentials
	* Login with a user with correct login credentials
* Give feedback to another user and Validate that the feedback was saved (When user has logged in to SayAtMe)
	* Login with correct username and password for SayAtMe
	* Search for Feedback receiver's username
	* Give feedback to the feedback receiver
	* Validate that the feedback was saved
* Give feedback to another user and Validate that the feedback was saved (When user has not logged in to SayAtMe)
	* Load the feedback receiver's url
	* Give feedback to the feedback receiver
	* Validate that the feedback was saved


### Features of tests done ###

* Tests run on multiple browsers:
	* Google Chrome
	* Mozilla Firefox
	* Microsoft Edge
	* Internet Explorer
* Tests done also includes 
	* Correct user login validation
	* Checking for the existence of Feedback receiver's url/username
	
### Steps to run the project ###

* Open the project with any IDE such as IntelliJ or Eclipse
* Download Chrome, Edge, Firefox and Internet Explorer Driver in your local system. (Preferably in a location: "C:/Users/Drivers" - inside the Driver folder)
* To run tests with all browsers (Chrome, Edge, Firefox and Internet Explorer) at once:
	* Right click on 'com.sayatmeselenium' package (inside the 'test' folder) and run it
* To run individual browser tests:
	* Right click on 'ChromeTest' inside the 'test' folder and run it
	* Right click on 'EdgeTest' inside the 'test' folder and run it
	* Right click on 'FirefoxTest' inside the 'test' folder and run it
	* Right click on 'InternetExplorerTest' inside the 'test' folder and run it